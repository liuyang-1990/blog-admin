FROM nginx
WORKDIR /usr/src/app/
COPY publish/  /usr/share/nginx/html/
RUN mkdir -p /etc/nginx/cert
RUN mkdir -p /etc/nginx/log
EXPOSE 80
EXPOSE 443
CMD ["nginx", "-g", "daemon off;"]