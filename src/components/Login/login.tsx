import React, { Component } from 'react';
import { Form, Tabs, Input, Icon, Checkbox, Button, Row, Col } from 'antd';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import './login.less';
import { Auth } from '../../infrastructure';
import UserLayout from '../../layouts/userLayout';
const { TabPane } = Tabs;
const FormItem = Form.Item;


@inject('login')
@observer
class LoginForm extends Component<any, any> {

    constructor(props) {
        super(props);
        this.state = {
            type: 'account',
            activeFields: ['UserName', 'Password'],
            autoLogin: true
        };
    }
    onSwitch = type => {
        this.setState({
            type,
        });
        if (type === "account") {
            this.setState({ activeFields: ['UserName', 'Password'] });
        } else if (type === "mobile") {
            this.setState({ activeFields: ['Mobile', 'captcha'] });
        }
    };

    changeAutoLogin = e => {
        this.setState({
            autoLogin: e.target.checked,
        });
    };


    handleSubmit = e => {
        e.preventDefault();
        const { activeFields } = this.state;
        const { form } = this.props;
        let { from } = this.props.location.state || { from: { pathname: "/" } };
        form.validateFields(activeFields, { force: true }, (err, values) => {
            if (!err) {
                this.props.login.loading = true;
                this.props.login.handleSubmit(values, this.state.autoLogin,from.pathname);
            }
        });
    };
    render() {
 
        const { getFieldDecorator } = this.props.form;
        const { autoLogin } = this.state;
        return (
            <UserLayout>
                <div className="main">
                    <div className="login">
                        <Form onSubmit={this.handleSubmit}>
                            <Tabs animated={false} activeKey={this.state.type} onChange={this.onSwitch}>
                                <TabPane tab="账号密码登录" key="account">
                                    <FormItem>
                                        {getFieldDecorator('UserName', {
                                            rules: [{ required: true, message: '用户名不能为空!' }]
                                        })(
                                            <Input size="large"
                                                prefix={<Icon type="user" className="prefixIcon" />}
                                                placeholder="请输入用户名"
                                            />
                                        )}
                                    </FormItem>
                                    <FormItem>
                                        {getFieldDecorator('Password', {
                                            rules: [{ required: true, message: '密码不能为空!' }]
                                        })(
                                            <Input.Password size="large"
                                                prefix={<Icon type="lock" className="prefixIcon" />}
                                                placeholder="请输入密码"
                                            />
                                        )}
                                    </FormItem>
                                </TabPane>
                                <TabPane tab="手机号登录" key="mobile">
                                    <FormItem>
                                        {getFieldDecorator('Mobile', {
                                            rules: [{ required: true, message: '手机号不能为空!' }]
                                        })(
                                            <Input size="large"
                                                prefix={<Icon type="mobile" className="prefixIcon" />}
                                                placeholder="请输入手机号"
                                            />
                                        )}
                                    </FormItem>
                                    <FormItem>
                                        <Row gutter={8}>
                                            <Col span={16}>
                                                {getFieldDecorator('captcha', {
                                                    rules: [{ required: true, message: '验证码不能为空!' }]
                                                })(
                                                    <Input size="large"
                                                        prefix={<Icon type="mail" className="prefixIcon" />}
                                                        placeholder="请输入验证码"
                                                    />
                                                )}
                                            </Col>
                                            <Col span={8}>
                                                <Button size="large" className="getCaptcha">
                                                    获取验证码
                                                </Button>
                                            </Col>
                                        </Row>
                                    </FormItem>
                                </TabPane>
                            </Tabs>
                            <Checkbox checked={autoLogin} onChange={this.changeAutoLogin}>
                                自动登录
                            </Checkbox>
                            <a style={{ float: 'right' }} href="javascripi:;">
                                忘记密码
                            </a>
                            <FormItem>
                                <Button size="large" loading={this.props.login.loading} className="submit" type="primary" htmlType="submit" > 登录</Button>
                            </FormItem>
                            <div className="other">
                                其他登录方式
                                <Icon type="qq" className="icon" theme="outlined" />
                                <Icon type="wechat" className="icon" theme="outlined" />
                                <Icon type="weibo" className="icon" theme="outlined" />
                                {/* <Link className="register" to="/user/register">
                                    <FormattedMessage id="app.login.signup" />
                                </Link> */}
                            </div>
                        </Form>
                    </div>
                </div>
            </UserLayout>
        )
    }
}

export default Form.create()(withRouter(LoginForm));