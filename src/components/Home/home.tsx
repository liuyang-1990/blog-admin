import React, { Component } from 'react';
import { Card, Col, Row } from 'antd';
import EchartsViews from './echartsViews';
import { withRouter } from 'react-router-dom';
import './home.less';

class Home extends Component<any, any>{
    render() {
        return (
            <div>
                <Row gutter={16}>
                    <Col span={8}>
                        <Card className='title' title="今日新增人数" bordered={false}>
                            Card content
                        </Card>
                    </Col>
                    <Col span={8}>
                        <Card className='title' title="本月新增人数" bordered={false}>
                            Card content
                        </Card>
                    </Col>
                    <Col span={8}>
                        <Card className='title' title="今日访问人数" bordered={false}>
                            Card content
                        </Card>
                    </Col>
                </Row>
                <br />
                <Row gutter={16}>
                    <Col span={8}>
                        <Card className='title' title="今日新增人数" bordered={false}>
                            Card content
                        </Card>
                    </Col>
                    <Col span={8}>
                        <Card className='title' title="本月新增人数" bordered={false}>
                            Card content
                        </Card>
                    </Col>
                    <Col span={8}>
                        <Card className='title' title="今日访问人数" bordered={false}>
                            Card content
                        </Card>
                    </Col>
                </Row>
                <br />
                <Row gutter={24}>
                    <Col span={24}>
                        <Card className='title' bordered={false}>
                            <div className="pb-m">
                                <h3>访问量统计</h3>
                                <small>最近30天用户访问量</small>
                            </div>
                            {/* <span className="card-tool"><Icon type="sync" /></span> */}
                            <EchartsViews />
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }

}

export default withRouter(Home);