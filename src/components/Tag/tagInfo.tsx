import React, { Component, Fragment } from 'react';
import { Form, Row, Col, Input, Button, Table, Modal,Tag  } from 'antd';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { ColumnProps } from 'antd/lib/table';
import { UserAction } from '../../model/action.model';
import TagModal from './tagModal';
import { toLocaleTimeString } from '../../infrastructure';
import '../common.less';

@inject('tag')
@observer
class TagInfo extends Component<any, any>{

    private formRef: any;
    //表格分页
    handleTableChange = (pagination, filters, sorter) => {
        let frmValues = this.props.form.getFieldsValue();
        this.props.tag.setTagName(frmValues.TagName);
        this.props.tag.pagination = pagination;
        this.props.tag.setPageIndex(pagination.current);
        this.props.tag.setPageSize(pagination.pageSize);
        this.props.tag.getPage();
    }
    //点击搜索按钮
    handleSearch = (e) => {
        e.preventDefault();
        this.props.tag.setPageIndex(1);
        let frmValues = this.props.form.getFieldsValue();
        this.props.tag.setTagName(frmValues.TagName);
        this.props.tag.getPage();
    }
    //清空搜索条件
    handleReset = () => {
        this.props.form.resetFields();
    }

    componentDidMount() {
        this.props.tag.getPage();
    }

    render() {
        const { form } = this.props;
        const { getFieldDecorator } = form;
        const columns: ColumnProps<any>[] = [
            {
                title: '标签名称',
                align: 'center',
                dataIndex: 'TagName',
                width: '25%',
                render: name=> <Tag color="#108ee9">{name}</Tag>
            }, {
                title: '描述',
                align: 'center',
                dataIndex: 'Description',
                width: '25%',
            }, {
                title: '创建时间',
                align: 'center',
                dataIndex: 'CreateTime',
                render: createTime => toLocaleTimeString(createTime)
            }, {
                title: '操作',
                align: 'center',
                dataIndex: '',
                key: 'x',
                render: (record) =>
                    <React.Fragment>
                        <Button type="primary" icon="edit" onClick={() => this.props.tag.handleClick(UserAction.Update, this.formRef, record)}>编辑</Button>
                        {` `}
                        <Button type="danger" icon="delete" onClick={() => this.props.tag.onShow(record.Id)}>删除</Button>
                    </React.Fragment>
            },
        ];

        return (
            <Fragment>
                <Form className="ant-advanced-search-form" onSubmit={this.handleSearch}>
                    <Row gutter={24}>
                        <Col span={8}>
                            <Form.Item label={"标签名称"}>
                                {getFieldDecorator('TagName')(<Input placeholder="请输入标签名称" />)}
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={24} style={{ textAlign: 'right' }}>
                            <Button type="primary" htmlType="submit" icon="search">搜索</Button>
                            <Button style={{ marginLeft: 8 }} icon="undo" onClick={this.handleReset}>
                                清空
                        </Button>
                        </Col>
                    </Row>
                </Form>
                <div className="search-result-list">
                    <div className="operator">
                        <Button type="primary" icon="plus" onClick={() => this.props.tag.handleClick(UserAction.Add, this.formRef)}>新增</Button>
                    </div>
                    <Table
                        columns={columns}
                        rowKey={"Id"}
                        bordered={true}
                        rowClassName={(record, index) => {
                            let className = 'light-row';
                            if (index % 2 === 1) className = 'dark-row';
                            return className;
                        }}
                        dataSource={this.props.tag.data}
                        pagination={this.props.tag.pagination}
                        loading={this.props.tag.loading}
                        onChange={this.handleTableChange}
                    />
                </div>
                <Modal
                    title={'删除标签'}
                    visible={this.props.tag.delVisible}
                    onOk={this.props.tag.onOk}
                    onCancel={this.props.tag.onHide}
                >
                    确定要删除该标签吗？
                </Modal>
                <TagModal
                    wrappedComponentRef={ref => this.formRef = ref}
                    visible={this.props.tag.visible}
                    onCancel={this.props.tag.onCancel}
                    onCreate={() => this.props.tag.onCreate(this.formRef)}
                    action={this.props.tag.action} />
            </Fragment>

        );
    }
}

export default Form.create()(withRouter(TagInfo));