import * as  React from 'react';
import { Form, Input, Modal } from 'antd';
import { UserAction } from '../../model/action.model';

const FormItem = Form.Item;

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 5 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
    },
};

const TagModal: any = Form.create({ name: "tag_in_modal" })(
    class extends React.Component<any, any> {

        render() {
            const { visible, form, action, onCreate, onCancel } = this.props;
            const { getFieldDecorator } = form;
            return (
                <Modal
                    title={action === UserAction.Add ? '新增标签' : "更新标签"}
                    visible={visible}
                    onOk={onCreate}
                    onCancel={onCancel}
                    maskClosable={false}
                >
                    <Form {...formItemLayout}>
                        <Form.Item label={"Id"} style={{ display: 'none' }}>
                            {getFieldDecorator('Id')(<Input type="text" />)}
                        </Form.Item>
                        <FormItem
                            label="标签名称"
                        >
                            {getFieldDecorator('TagName', {
                                rules: [{
                                    required: true, message: '请输入标签名称',
                                }],
                            })(
                                <Input type="text" />
                            )}
                        </FormItem>
                        <Form.Item label={"描述"} className="collection-create-form_last-form-item">
                            {getFieldDecorator('Description')(<Input type="text" />)}
                        </Form.Item>
                    </Form>
                </Modal>
            )
        }
    }
);

export default TagModal;
