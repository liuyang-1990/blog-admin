import React, { Component } from 'react';
import { Button, Result } from 'antd';
import config from './typeConfig';
import { withRouter } from 'react-router-dom';

interface IExceptionProps {
    type: string
}


class Exception extends Component<IExceptionProps & any, any>{


    render() {
        const { type } = this.props;
        const pageType = type in config ? type : '404';
        const id = this.props.match.params.id;
        return (
            <Result
                status="404"
                title={config[pageType].title}
                subTitle={config[pageType].desc}
                extra={
                    <Button type="primary" onClick={() => this.props.history.push("/")}>
                        回到首页
                  </Button>

                } />

        );
    }
}

export default withRouter(Exception);