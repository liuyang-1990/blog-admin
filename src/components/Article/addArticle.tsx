import * as React from 'react';
import { Form, Input, Button, Select, Checkbox, Row, Col, Upload, Icon, Modal, message } from 'antd';
import { withRouter } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import AddTagModal from './addTagModal';
import ArticleState from '../../states/article.state';
import BraftEditor from 'braft-editor';
import ColorPicker from 'braft-extensions/dist/color-picker';
import { RcFile } from 'antd/lib/upload/interface';
import { uploadIamge } from '../../infrastructure/utils/uploadImage';
import './article.less';
import 'braft-editor/dist/index.css';
import 'braft-extensions/dist/color-picker.css'

const Option = Select.Option;
const { TextArea } = Input;

BraftEditor.use(ColorPicker({
    theme: 'light' // 支持dark和light两种主题，默认为dark
}));

@inject('article')
@observer
class AddArticle extends React.Component<any, any>{
    constructor(props: any) {
        super(props);
        this.state = {
            editorState: null,
            fileList: [],
            previewVisible: false,
            previewImage: ''
        }
    }

    async componentDidMount() {
        this.props.article.getAllCategoryInfos();
        let params = new URLSearchParams(this.props.location.search);
        //说明是编辑
        if (params.get("postid")) {
            const html = await this.props.article.getDetail(params.get("postid"));
            this.props.form.setFieldsValue({
                Content: BraftEditor.createEditorState(html)
            })
        }

    }

    componentWillUnmount() {
        this.props.article.resetFormValue();
    }
    onOk = (ids: Array<number>) => {
        let tag: string = this.props.form.getFieldValue("Tags");
        if (tag && !tag.endsWith(",")) {
            tag = tag + ",";
        }
        let selected = ids.join(",");
        this.props.form.setFieldsValue({ Tags: tag + selected });
        this.props.article.visible = false;
    }

    handleSubmit = (e) => {
        e.preventDefault();
        let params = new URLSearchParams(this.props.location.search);
        this.props.form.validateFields((error, values) => {
            if (!error) {
                let html = this.state.editorState.toHTML();
                values = Object.assign(values, {
                    Content: html,
                    Id: params.get("postid") ? params.get("postid") : 0,
                    ImageUrl: this.state.fileList.length > 0 && this.state.fileList[0].response.url
                });
                this.props.article.addArticle(values, params.get("postid"));
            }
        })
    }
    onChange = (content) => {
        this.setState({
            editorState: content
        });
    }

  
    //上传图片前，检验是否符合规则
    beforeUpload = (file: RcFile) => {
        const isImage = file.type.startsWith("image/");
        if (!isImage) {
            message.error('图片格式不正确!');
        }
        const isLt2M = file.size / 1024 / 1024 < 2;
        if (!isLt2M) {
            message.error('图片超过2M了!');
        }
        return isImage && isLt2M;
    }
    //取消预览
    handleCancel = () => this.setState({ previewVisible: false });

    //预览
    handlePreview = file => {
        this.setState({
            previewImage: file.url || file.thumbUrl,
            previewVisible: true,
        });
    };

    //自定义上传方法
    customRequest = (param) => {
        uploadIamge(param.file, param.onProgress, param.onSuccess, param.onError);
    }

    handleChange = ({ fileList }) => this.setState({ fileList });


    onCancel = () => {
        this.props.history.push("/article/list");
    }

    uploadFn = (param) => {
        uploadIamge(param.file, param.progress, param.success, param.error);
    }
    render() {
        const store: ArticleState = this.props.article;
        const { getFieldDecorator } = this.props.form;
        const selectBefore = getFieldDecorator('IsOriginal', { initialValue: store.form_isOriginal })(
            <Select style={{ width: 70 }}>
                <Option value="1">原创</Option>
                <Option value="0">转载</Option>
            </Select>
        );
        const buttonlayout = {
            wrapperCol: {
                xs: { span: 24, offset: 0 },
                sm: { span: 23, offset: 0 },
            }
        };

        const uploadButton = (
            <div>
                <Icon type="plus" />
                <div className="ant-upload-text">上传</div>
            </div>
        );


        const { previewVisible, previewImage, fileList } = this.state;
        return (
            <div className="article_form">
                <Form onSubmit={this.handleSubmit}>
                    <Form.Item label="标题">
                        {getFieldDecorator('Title', {
                            rules: [{ required: true, message: '请输入标题' }],
                            initialValue: store.form_title
                        })(
                            <Input
                                size="large"
                                addonBefore={selectBefore}
                                placeholder="请输入标题"
                                maxLength={200}
                            />
                        )}
                    </Form.Item>
                    <Form.Item label="内容">
                        {getFieldDecorator('Content', {
                            validateTrigger: "onSubmit",
                            rules: [{
                                required: true,
                                validator: (_, value, callback) => {
                                    if (value.isEmpty()) {
                                        callback('请输入文章内容')
                                    } else {
                                        callback()
                                    }
                                }
                            }]
                        })(
                            <BraftEditor
                                media={{ uploadFn: this.uploadFn }}
                                className="braft-editor"
                                onChange={this.onChange}
                                placeholder="请输入正文内容" />
                        )}
                    </Form.Item>
                    {
                        store.categoryInfos.length > 0 && <Form.Item label="分类">
                            {getFieldDecorator('Category', {
                                initialValue: store.form_category,
                            })(
                                <Checkbox.Group style={{ width: '100%' }}>
                                    <Row>
                                        {
                                            store.categoryInfos.map(c => {
                                                return (<Col key={c.Id} span={8}>
                                                    <Checkbox value={c.Id}>{c.Name}</Checkbox>
                                                </Col>)
                                            })
                                        }
                                    </Row>
                                </Checkbox.Group>)}
                        </Form.Item>
                    }
                    <Form.Item colon={false} label={
                        // eslint-disable-next-line 
                        <span className="tags_sp">标签(多个关键字之间用“,”分隔){` `} <a href="javascript:;" onClick={store.onShow}>插入已有标签</a>
                        </span>
                    }>
                        {getFieldDecorator("Tags", { initialValue: store.form_tags, })(
                            <Input size="large" type="primary" placeholder="请输入标签"/>)
                        }
                    </Form.Item>
                    <Form.Item label="摘要">
                        {getFieldDecorator('Abstract', {
                            rules: [{ required: true, message: '请输入摘要' }],
                            initialValue: store.form_abstract
                        })(<TextArea placeholder="请输入摘要" maxLength={500} style={{ resize: 'none' }} autosize={{ minRows: 2, maxRows: 6 }} />)}
                    </Form.Item>
                    <Form.Item label="摘要右侧图片">
                        <div className="clearfix">
                            <Upload
                                accept="image/*"
                                beforeUpload={this.beforeUpload}
                                listType="picture-card"
                                customRequest={this.customRequest}
                                onPreview={this.handlePreview}
                                onChange={this.handleChange}
                            >
                                {fileList.length >= 1 ? null : uploadButton}
                            </Upload>
                            <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
                                <img alt="avatar" style={{ width: '100%' }} src={previewImage} />
                            </Modal>
                        </div>
                    </Form.Item>
                    <Form.Item  {...buttonlayout}>
                        <Button
                            type="primary"
                            onClick={() => store.setStatus(1)}
                            style={{ width: 80 }}
                            htmlType="submit">
                            发表文章
                        </Button>
                        <Button style={{ marginLeft: 8, width: 80 }}
                            onClick={() => store.setStatus(0)}
                            type="primary"
                            htmlType="submit">
                            存为草稿
                        </Button>
                        <Button style={{ marginLeft: 8, width: 80 }}
                            onClick={this.onCancel}
                            type="primary">
                            取消
                        </Button>
                    </Form.Item>
                </Form>
                <AddTagModal
                    visible={store.visible}
                    loading={store.loading}
                    data={store.data}
                    pagination={store.pagination}
                    onCancel={store.onCancel}
                    onOk={this.onOk}
                />
            </div >
        );
    }

}
export default Form.create()(withRouter(AddArticle));