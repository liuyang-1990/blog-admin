import * as React from 'react';
import { Form, Row, Col, Button, Table, DatePicker, Modal } from 'antd';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { ColumnProps } from 'antd/lib/table';
import { toLocaleTimeString } from '../../infrastructure';
import '../common.less';

@inject('article')
@observer
class ArticleList extends React.Component<any, any>{

    handleSearch = (e) => {
        e.preventDefault();
        this.props.article.setPageIndex(1);
        this.props.article.getPage();
    }

    //表格分页
    handleTableChange = (pagination, filters, sorter) => {
        this.props.article.articlePagination = pagination;
        this.props.article.setPageIndex(pagination.current);
        this.props.article.setPageSize(pagination.pageSize);
        this.props.article.getPage();
    }
    //清空搜索条件
    handleReset = () => {
        this.props.form.resetFields();
    }
    handleEdit = (id: number) => {
        this.props.history.push({ pathname: '/article/addArticle', search: "?postid=" + id });
    }
    componentDidMount() {
        this.props.article.getPage();
    }
    sonChange = (value, dateString) => {
        this.props.article.search_startTime = dateString;
    }
    eonChange = (value, dateString) => {
        this.props.article.serach_endTime = dateString;
    }
    render() {
        const dateFormat = 'YYYY/MM/DD';
        const { form, article } = this.props;
        const { getFieldDecorator } = form;
        const columns: ColumnProps<any>[] = [
            {
                title: '标题',
                align: 'center',
                dataIndex: 'Title',
                width: '40%',
            }, {
                title: '状态',
                align: 'center',
                render: status => status === 1 ? "发布" : "草稿",
                dataIndex: 'Status',
                width: '10%',
            }, {
                title: '是否原创',
                align: 'center',
                render: IsOriginal => IsOriginal === 1 ? "原创" : "转载",
                dataIndex: 'IsOriginal',
                width: '10%',
            }, {
                title: '创建时间',
                align: 'center',
                dataIndex: 'CreateTime',
                render: createTime => toLocaleTimeString(createTime)
            },
            {
                title: '操作',
                align: 'center',
                dataIndex: '',
                key: 'x',
                render: (record) =>
                    <React.Fragment>
                        <Button type="primary" icon="edit" onClick={() => this.handleEdit(record.Id)}>编辑</Button>{` `}
                        <Button type="danger" icon="delete" onClick={() => this.props.article.delonShow(record.Id)}>删除</Button>
                    </React.Fragment>
            },
        ];

        return (
            <React.Fragment>
                <Form className="ant-advanced-search-form" onSubmit={this.handleSearch}>
                    <Row gutter={24}>
                        <Col span={8}>
                            <Form.Item label={"起始时间"}>
                                {getFieldDecorator('StartTime')(<DatePicker onChange={this.sonChange} format={dateFormat} style={{ width: '100%' }} />)}
                            </Form.Item>
                        </Col>
                        <Col span={8}>
                            <Form.Item label={"结束时间"}>
                                {getFieldDecorator('EndTime')(<DatePicker onChange={this.eonChange} format={dateFormat} style={{ width: '100%' }} />)}
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={24} style={{ textAlign: 'right' }}>
                            <Button type="primary" icon="search" htmlType="submit">搜索</Button>
                            <Button style={{ marginLeft: 8 }} icon="undo" onClick={this.handleReset}>
                                清空
                    </Button>
                        </Col>
                    </Row>
                </Form>
                <div className="search-result-list">
                    <Table
                        columns={columns}
                        rowKey={"Id"}
                        bordered={true}
                        rowClassName={(record, index) => {
                            let className = 'light-row';
                            if (index % 2 === 1) className = 'dark-row';
                            return className;
                        }}
                        dataSource={article.articleData}
                        pagination={article.articlePagination}
                        loading={article.articleLoading}
                        onChange={this.handleTableChange}
                    />
                </div>
                <Modal
                    title={'删除文章'}
                    visible={article.delVisible}
                    onOk={article.onOk}
                    onCancel={article.onHide}
                    maskClosable={false}
                >
                    确定要删除该文章吗？
                </Modal>
            </React.Fragment>
        )
    }
}

export default Form.create()(withRouter(ArticleList));