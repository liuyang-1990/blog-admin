import * as React from 'react';
import { Modal, Table, Tag } from 'antd';
import { ColumnProps } from 'antd/lib/table';

interface IAddTagModalProps {
    visible: boolean,
    data: any,
    pagination: any,
    loading: boolean,
    onOk: (ids) => void,
    onCancel: () => void
}

const columns: ColumnProps<any>[] = [
    {
        title: '标签名称',
        align: 'center',
        dataIndex: 'TagName',
        width: '50%',
        render: name=> <Tag color="#2db7f5">{name}</Tag>
    }, {
        title: '描述',
        align: 'center',
        dataIndex: 'Description'
    }
];

export default class AddTagModal extends React.Component<IAddTagModalProps, any> {

    constructor(props) {
        super(props);
        this.state = {
            selectedRowKeys: []
        };
    }


    onOk = () => {
        const { selectedRowKeys } = this.state;
        this.props.onOk(selectedRowKeys);
        this.setState({ selectedRowKeys: [] });

    }

    onCancel = () => {
        this.props.onCancel();
        this.setState({ selectedRowKeys: [] });
    }

    render() {
        const { data, pagination, loading, visible } = this.props;

        const { selectedRowKeys } = this.state;
        //checkbox
        const rowSelection = {
            selectedRowKeys,
            onChange: (selectedRowKeys) => {
                this.setState({ selectedRowKeys: selectedRowKeys });
            }
        };
        return (
            <Modal
                title={"选择标签"}
                visible={visible}
                onOk={this.onOk}
                onCancel={this.onCancel}
                maskClosable={false}
            >
                <Table
                    rowSelection={rowSelection}
                    columns={columns}
                    rowKey={"Id"}
                    bordered={true}
                    rowClassName={(record, index) => {
                        let className = 'light-row';
                        if (index % 2 === 1) className = 'dark-row';
                        return className;
                    }}
                    dataSource={data}
                    pagination={pagination}
                    loading={loading}
                />
            </Modal>
        )
    }
}

