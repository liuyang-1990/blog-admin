import * as  React from 'react';
import { Form, Input, Modal } from 'antd';
import { UserAction } from '../../model/action.model';

const FormItem = Form.Item;

const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 5 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
    },
};

const CategoryModal: any = Form.create({ name: "category_in_modal" })(
    class extends React.Component<any, any> {

        render() {
            const { visible, form, action, onCreate, onCancel } = this.props;
            const { getFieldDecorator } = form;
            return (
                <Modal
                    title={action === UserAction.Add ? '新增分类' : "更新分类"}
                    visible={visible}
                    onOk={onCreate}
                    onCancel={onCancel}
                    maskClosable={false}
                   // destroyOnClose={true}
                >
                    <Form {...formItemLayout}>
                        <Form.Item label={"Id"} style={{ display: 'none' }}>
                            {getFieldDecorator('Id')(<Input type="text" />)}
                        </Form.Item>
                        <FormItem
                            label="分类名称"
                        >
                            {getFieldDecorator('CategoryName', {
                                rules: [{
                                    required: true, message: '请输入分类名称',
                                }],
                            })(
                                <Input type="text" />
                            )}
                        </FormItem>
                        <Form.Item label={"描述"} className="collection-create-form_last-form-item">
                            {getFieldDecorator('Description')(<Input type="text" />)}
                        </Form.Item>
                    </Form>
                </Modal>
            )
        }
    }
);

export default CategoryModal;
