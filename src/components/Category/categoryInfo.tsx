import React, { Component, Fragment } from 'react';
import { Form, Row, Col, Input, Button, Table, Modal } from 'antd';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { ColumnProps } from 'antd/lib/table';
import CategoryModal from './categoryModal';
import { UserAction } from '../../model/action.model';
import { toLocaleTimeString } from '../../infrastructure';
import '../common.less';

@inject('category')
@observer
class CategoryInfo extends Component<any, any>{

    private formRef: any;
    //表格分页
    handleTableChange = (pagination, filters, sorter) => {
        let frmValues = this.props.form.getFieldsValue();
        this.props.category.setCategoryName(frmValues.CategoryName);
        this.props.category.pagination = pagination;
        this.props.category.setPageIndex(pagination.current);
        this.props.category.setPageSize(pagination.pageSize);
        this.props.category.getPage();
    }
    //点击搜索按钮
    handleSearch = (e) => {
        e.preventDefault();
        this.props.category.setPageIndex(1);
        let frmValues = this.props.form.getFieldsValue();
        this.props.category.setCategoryName(frmValues.CategoryName);
        this.props.category.getPage();
    }
    //清空搜索条件
    handleReset = () => {
        this.props.form.resetFields();
    }

    componentDidMount() {
        this.props.category.getPage();
    }

    render() {
        const { form } = this.props;
        const { getFieldDecorator } = form;
        const columns: ColumnProps<any>[] = [
            {
                title: '分类名称',
                align: 'center',
                dataIndex: 'CategoryName',
                width: '25%',
            }, {
                title: '描述',
                align: 'center',
                dataIndex: 'Description',
                width: '25%',
            }, {
                title: '创建时间',
                align: 'center',
                dataIndex: 'CreateTime',
                render: createTime => toLocaleTimeString(createTime)
            }, {
                title: '操作',
                align: 'center',
                dataIndex: '',
                key: 'x',
                render: (record) =>
                    <React.Fragment>
                        <Button type="primary" icon="edit" onClick={() => this.props.category.handleClick(UserAction.Update, this.formRef, record)}>编辑</Button>
                        {` `}
                        <Button type="danger" icon="delete" onClick={() => this.props.category.onShow(record.Id)}>删除</Button>
                    </React.Fragment>
            },
        ];

        return (
            <Fragment>
                <Form className="ant-advanced-search-form" onSubmit={this.handleSearch}>
                    <Row gutter={24}>
                        <Col span={8}>
                            <Form.Item label={"分类名称"}>
                                {getFieldDecorator('CategoryName')(<Input placeholder="请输入分类名称" />)}
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={24} style={{ textAlign: 'right' }}>
                            <Button type="primary" icon="search" htmlType="submit">搜索</Button>
                            <Button style={{ marginLeft: 8 }} icon="undo" onClick={this.handleReset}>
                                清空
                        </Button>
                        </Col>
                    </Row>
                </Form>
                <div className="search-result-list">
                    <div className="operator">
                        <Button type="primary" icon="plus" onClick={() => this.props.category.handleClick(UserAction.Add, this.formRef)}>新增</Button>
                    </div>
                    <Table
                        columns={columns}
                        rowKey={"Id"}
                        bordered={true}
                        rowClassName={(record, index) => {
                            let className = 'light-row';
                            if (index % 2 === 1) className = 'dark-row';
                            return className;
                        }}
                        dataSource={this.props.category.data}
                        pagination={this.props.category.pagination}
                        loading={this.props.category.loading}
                        onChange={this.handleTableChange}
                    />
                </div>
                <Modal
                    title={'删除分类'}
                    visible={this.props.category.delVisible}
                    onOk={this.props.category.onOk}
                    onCancel={this.props.category.onHide}
                >
                    确定要删除该分类吗？
                </Modal>
                <CategoryModal
                    wrappedComponentRef={ref => this.formRef = ref}
                    visible={this.props.category.visible}
                    onCancel={this.props.category.onCancel}
                    onCreate={() => this.props.category.onCreate(this.formRef)}
                    action={this.props.category.action} />
            </Fragment>

        );
    }
}

export default Form.create()(withRouter(CategoryInfo));