import * as  React from 'react';
import { Form, Input, Modal, Select } from 'antd';
import { UserAction } from '../../model/action.model';
import { FormComponentProps } from 'antd/lib/form';
import { IUserInfo } from '../../model/user.model';


interface IUserFormProps extends FormComponentProps {
    visible: boolean,
    action: UserAction,
    onCreate: () => void,
    onCancel: () => void,
    onChange: (e) => void,
    formFields: IUserInfo
}

const FormItem = Form.Item;
const Option = Select.Option;
const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 5 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
    },
}

class UserForm extends React.Component<IUserFormProps, any> {
    render() {
        const { action, visible, form, onCreate, onCancel } = this.props;
        const { getFieldDecorator } = form;
        return (
            <Modal
                title={action === UserAction.Add ? '新增用户' : "更新用户"}
                visible={visible}
                onOk={onCreate}
                onCancel={onCancel}
                maskClosable={false}
            >
                <Form {...formItemLayout}>
                    <Form.Item label={"Id"} style={{ display: 'none' }}>
                        {getFieldDecorator('Id')(<Input type="text" />)}
                    </Form.Item>
                    <FormItem label="用户名" >
                        {getFieldDecorator('UserName', {
                            rules: [{
                                required: true, message: '请输入用户名',
                            }],
                        })(
                            <Input type="text" />
                        )}
                    </FormItem>
                    <Form.Item label={"状态"}>
                        {getFieldDecorator('Status', {
                            initialValue: "1"
                        })(
                            <Select>
                                <Option value="1">启用</Option>
                                <Option value="0">禁用</Option>
                            </Select>)}
                    </Form.Item>
                    <Form.Item label={"角色"} className="collection-create-form_last-form-item">
                        {getFieldDecorator('Role', {
                            initialValue: "0"
                        })(
                            <Select>
                                <Option value="1">管理员</Option>
                                <Option value="0">游客</Option>
                            </Select>)}
                    </Form.Item>
                </Form>
            </Modal>
        );
    }
}

const UserModal = Form.create<IUserFormProps>({
    name: 'user_in_modal',
    mapPropsToFields(props) {
        return {
            Id: Form.createFormField({
                value: props.formFields.Id
            }),
            UserName: Form.createFormField({
                value: props.formFields.UserName
            }),
            Status: Form.createFormField({
                value: props.formFields.Status.toString()
            }),
            Role: Form.createFormField({
                value: props.formFields.Role.toString()
            })
        };
    },
    onValuesChange(props, changedValues) {
        props.onChange(changedValues);
    }
})(UserForm);


export default UserModal;
