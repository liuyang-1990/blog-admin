import React, { Component, Fragment } from 'react';
import { Form, Row, Col, Input, Button, Select, Table,message } from 'antd';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { ColumnProps } from 'antd/lib/table';
import UserModal from './userModal';
import { UserAction } from '../../model/action.model';
import { toLocaleTimeString } from '../../infrastructure';
import { IUserInfo } from '../../model/user.model';
import '../common.less';

const Option = Select.Option;


@inject('user')
@observer
class UserInfo extends Component<any, any>{

    private formRef: any;
    constructor(props) {
        super(props);
        this.state = {
            selectedRowKeys: [],
            action: UserAction.Add,
            fields: {
                Role: 1,
                Status: 1
            }
        }
    }


    //表格分页
    handleTableChange = (pagination, filters, sorter) => {
        let frmValues = this.props.form.getFieldsValue();
        this.props.user.setUserName(frmValues.UserName);
        this.props.user.pagination = pagination;
        this.props.user.setPageIndex(pagination.current);
        this.props.user.setPageSize(pagination.pageSize);
        this.props.user.getUserPage();
    }
    //点击搜索按钮
    handleSearch = (e) => {
        e.preventDefault();
        this.props.user.setPageIndex(1);
        let frmValues = this.props.form.getFieldsValue();
        this.props.user.setUserName(frmValues.UserName);
        this.props.user.getUserPage();
    }
    //清空搜索条件
    handleReset = () => {
        this.props.form.resetFields();
    }

    //下拉框改变
    handleChange = (value) => {
        this.props.user.setPageIndex(1);
        this.props.user.status = value;
        this.props.user.getUserPage();
    }

    handleChangeStatus = async () => {
        const { selectedRowKeys } = this.state;
        if (selectedRowKeys.length === 0) {
            message.info("至少选择一个！");
            return;
        }
        const res = await this.props.user.handleChangeStatus(selectedRowKeys);
        if (res) {
            this.setState({
                selectedRowKeys: []
            });
        }
    }

    handleUserClick = (action: UserAction, record?: any) => {
        this.setState({ action: action });
        this.props.user.onToggle();
        if (action === UserAction.Update) {
            const userInfo: IUserInfo = {
                Id: record.Id,
                UserName: record.UserName,
                Role: record.Role,
                Status: record.Status
            };
            this.setState({ fields: userInfo });
        } else {
            this.setState({ fields: { Role: 1, Status: 1 } });
        }
    }

    onCreate = () => {
        this.formRef.props.form.validateFields((err, values) => {
            if (err) {
                return;
            }
            this.props.user.addUser(values, this.state.action);
        })
    }

    onCancel = () => {
        this.props.user.onToggle();
    }
    componentDidMount() {
        this.props.user.getUserPage();
    }

    handleFormChange = changedFields => {
        this.setState(({ fields }) => ({
            fields: { ...fields, ...changedFields },
        }));
    };
    render() {
        const { form } = this.props;
        const { getFieldDecorator } = form;
        const columns: ColumnProps<any>[] = [
            {
                title: '用户名',
                align: 'center',
                dataIndex: 'UserName',
                width: '20%',
            }, {
                title: '角色',
                align: 'center',
                dataIndex: 'Role',
                render: role => role === 1 ? "管理员" : "游客",
                width: '20%',
            }, {
                title: '状态',
                align: 'center',
                render: status => status === 1 ? "启用" : "禁用",
                dataIndex: 'Status',
                width: '20%',
            }, {
                title: '创建时间',
                align: 'center',
                dataIndex: 'CreateTime',
                render: createTime => toLocaleTimeString(createTime)
            },
            {
                title: '操作',
                align: 'center',
                dataIndex: '',
                key: 'x',
                render: (record: IUserInfo) => record.UserName !== "admin" &&
                    <React.Fragment>
                        <Button type="primary" icon="edit" onClick={() => this.handleUserClick(UserAction.Update, record)}>编辑</Button>
                    </React.Fragment>
            },
        ];
        const { selectedRowKeys, fields, action } = this.state;
        const store = this.props.user;
        //checkbox
        const rowSelection = {
            selectedRowKeys,
            onChange: (selectedRowKeys) => {
                this.setState({ selectedRowKeys: selectedRowKeys });
            },
            getCheckboxProps: record => ({
                disabled: record.UserName === 'admin', // Column configuration not to be checked
                name: record.name,
            }),
        };
        return (
            <Fragment>
                <Form className="ant-advanced-search-form" onSubmit={this.handleSearch}>
                    <Row gutter={24}>
                        <Col span={8}>
                            <Form.Item label={"用户名"}>
                                {getFieldDecorator('UserName')(<Input placeholder="请输入用户名" />)}
                            </Form.Item>
                        </Col>
                        <Col span={8}>
                            <Form.Item label={"状态"}>
                                {getFieldDecorator('Status', {
                                    initialValue: "1"
                                })(
                                    <Select onChange={this.handleChange}>
                                        <Option value="1">启用</Option>
                                        <Option value="0">禁用</Option>
                                    </Select>)}
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={24} style={{ textAlign: 'right' }}>
                            <Button type="primary" htmlType="submit" icon="search">搜索</Button>
                            <Button style={{ marginLeft: 8 }} icon="undo" onClick={this.handleReset}>
                                清空
                        </Button>
                        </Col>
                    </Row>
                </Form>
                <div className="search-result-list">
                    <div className="operator">
                        <Button type="primary" icon="plus"
                            onClick={() => this.handleUserClick(UserAction.Add)}>新增</Button>
                        {` `}
                        <Button type="primary" icon={this.props.user.status === "1" ? "lock" : "unlock"}
                            onClick={this.handleChangeStatus}>{this.props.user.status === "1" ? "禁用" : "启用"}</Button>
                    </div>
                    <Table
                        columns={columns}
                        rowKey={"Id"}
                        bordered={true}
                        rowClassName={(record, index) => {
                            let className = 'light-row';
                            if (index % 2 === 1) className = 'dark-row';
                            return className;
                        }}
                        rowSelection={rowSelection}
                        dataSource={this.props.user.data}
                        pagination={this.props.user.pagination}
                        loading={this.props.user.loading}
                        onChange={this.handleTableChange}
                    />
                </div>
                <UserModal
                    wrappedComponentRef={ref => this.formRef = ref}
                    visible={store.visible}
                    onCancel={this.onCancel}
                    onCreate={this.onCreate}
                    action={action}
                    onChange={this.handleFormChange}
                    formFields={fields}
                />
            </Fragment>

        );
    }
}

export default Form.create()(withRouter(UserInfo));