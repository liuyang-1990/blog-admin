import React from 'react';
import { Redirect, Route } from "react-router-dom";
import storageUtils from "./utils/storageUtils";

export default function PrivateRoute({ component: Component, ...rest }) {
  return (
    <Route {...rest}
      render={props =>
        storageUtils.getItem("access_token") ? (
          <Component {...props} />
        ) : (
            <Redirect
              to={{
                pathname: "/login",
                state: { from: props.location }
              }}
            />
          )
      }
    />
  );
}