import moment from 'moment';

export enum TimeStringFormat {
    STANDARD_TIME = "YYYY-MM-DD HH:mm:ss",

    STANDARD_DATE = "YYYY-MM-DD"

}

export function toLocaleTimeString(time: string, format: TimeStringFormat = TimeStringFormat.STANDARD_TIME): string {
    return moment(time).format(format);
}