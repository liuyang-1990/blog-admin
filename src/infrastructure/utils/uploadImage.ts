import { Post } from "../../http/httpClient";

export function uploadIamge(file, progress, success, error) {
    const fd = new FormData();
    fd.append('file', file);
    Post("uploadApi", fd, false, {
        headers: {
            'Content-Type': 'multipart/form-data',
            'Accept': 'application/json'
        },
        onUploadProgress: (event: { loaded: number, total: number }) => {
            progress(Math.round((event.loaded / event.total) * 100), file);
        }
    }).then(res => {
        success({ url: res.data.WebUrl }, file)
    }).catch(err => {
        console.log(err);
        error({ msg: 'unable to upload.' });
    });
}