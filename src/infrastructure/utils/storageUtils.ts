let storageUtils = {

    getItem: function (name: string) {
        return localStorage.getItem(name) ? localStorage.getItem(name) : sessionStorage.getItem(name)
    }


}


export default storageUtils;
