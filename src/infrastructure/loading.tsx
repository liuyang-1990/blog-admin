import React, { Component } from 'react';
import * as Loadable from "react-loadable";
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import { Spin } from 'antd';

export default class LoadingPage extends Component<Loadable.LoadingComponentProps>{
	componentWillMount() {
		NProgress.start();
	}

	componentWillUnmount() {
		NProgress.done();
	}

	render() {
		return <Spin size='large' delay={500}/>
	}
}


