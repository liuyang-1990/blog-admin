import * as React from 'react';
import { Redirect } from 'react-router-dom';
import storageUtils from './utils/storageUtils';

//for login page to use
export function Auth(): <T extends React.ComponentClass>(Comp: T) => T {
    return (Comp: any): any => {
        class WrapperComponent extends Comp {
            render() {
                const auth = storageUtils.getItem("access_token");
                return (
                    auth ? React.createElement(Redirect, { to: "/" }) : React.createElement(Comp, this.props)
                )
            }
        }
        return WrapperComponent
    }
}
