import { action, observable } from 'mobx';
import UserService from '../services/user.service';
import { message } from 'antd';
 import { menus } from '../config/menus.conf';
import { MenuDataItem } from '@ant-design/pro-layout';
export default class BaseState {
    private userService: UserService;
    constructor() {
        this.userService = new UserService();
    }
    @observable visible: boolean = false;
    @observable collapsed: boolean = false;


    menuList=menus as MenuDataItem[] ;
    @action.bound
    handleCancel() {
        this.visible = false;
    }
    @action.bound
    handleOk(formRef: any) {
        formRef.props.form.validateFields((err, values) => {
            if (err) {
                return;
            }
            this.UpdatePassword(values);
        });
    }
    @action.bound
    onShow() {
        this.visible = true;
    }

    @action.bound
    async UpdatePassword(params) {
        const userName = localStorage.getItem("userName");
        params = Object.assign(params, {
            UserName: userName
        });
        const res = await this.userService.ChangePassword(params);
        if (res && res.data) {
            switch (res.data.Status) {
                case "0":
                    message.warn("修改成功！");
                    this.visible = false;
                    break;
                case "1":
                    message.warn("失败！请稍后再试！");
                    break;
                case "2":
                    message.warn("旧密码不正确！");
                    break;
                default:
                    break;

            }
        }
    }

    @action.bound
    toggle() {
        this.collapsed = !this.collapsed
    }

}