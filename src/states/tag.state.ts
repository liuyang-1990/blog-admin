import { action, observable } from 'mobx';
import { message } from 'antd';
import TagService from '../services/tag.service';
import { UserAction } from '../model/action.model';


export default class TagState {
    private tagService: TagService;
    constructor() {
        this.tagService = new TagService();
    }

    private formRef: any;
    //搜索参数
    @observable pageIndex: number = 1;
    @observable pageSize: number = 10;
    @observable tagName: string = '';
    //表格
    @observable data: Array<any> = [];
    @observable loading: boolean = false;
    @observable pagination: any = {};

    @observable visible: boolean = false;
    @observable action: UserAction = UserAction.Add;

    @observable delVisible: boolean = false;
    @observable id: number = 0;

    @action.bound
    async getPage() {
        this.loading = true;
        const res = await this.tagService.getPage(this.pageIndex, this.pageSize, this.tagName);
        this.loading = false;
        if (res && res.data) {
            const pagination = { ...this.pagination };
            pagination.Toal = res.data.TotalRows;
            pagination.showQuickJumper = true;
            pagination.showSizeChanger = true;
            pagination.showTotal = (total, range) => `${range[0]}-${range[1]} of ${total}`;
            this.data = res.data.Rows;
            this.pagination = pagination;
            this.visible = false;
            this.delVisible = false;
            this.formRef && this.formRef.props.form.resetFields();
        }

    }

    setTagName(tagName: string) {
        this.tagName = tagName;
    }

    setPageIndex(current: number) {
        this.pageIndex = current;
    }

    setPageSize(size: number) {
        this.pageSize = size;
    }

    @action.bound
    onCreate(formRef) {
        const form = formRef.props.form;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }
            this.addTag(values, this.action);
        });
    }

    @action.bound
    onCancel() {
        this.visible = false;
        this.formRef && this.formRef.props.form.resetFields();
    }
    @action.bound
    handleClick(userAction: UserAction, formRef: any, record: any) {
        this.visible = true;
        this.action = userAction;
        this.formRef = formRef;
        if (userAction === UserAction.Update) {
            formRef && formRef.props.form.setFieldsValue({
                Id: record.Id,
                TagName: record.TagName,
                Description: record.Description
            });
        }
    }

    @action.bound
    async addTag(params, userAction: UserAction) {
        let res;
        switch (userAction) {
            case UserAction.Add:
                res = await this.tagService.addTag(params);
                break;
            case UserAction.Update:
                res = await this.tagService.UpdateTag(params);
                break;
            default:
                break;
        }
        if (res && res.data) {
            switch (res.data.Status) {
                case "0":
                    await this.getPage();
                    break;
                case "1":
                    message.warn("失败！请稍后再试！");
                    break;
                case "2":
                    message.warn("分类已经存在！");
                    break;
                default:
                    break;

            }
        }
    }
    @action.bound
    onShow(id: number) {
        this.id = id;
        this.delVisible = true;
    }

    @action.bound
    onHide() {
        this.delVisible = false;
    }

    @action.bound
    async onOk() {
        const res = await this.tagService.deleteTag(this.id);
        if (res && res.data) {
            switch (res.data.Status) {
                case "0":
                    message.warn("删除成功！");
                    await this.getPage();
                    break;
                case "1":
                    message.warn("失败！请稍后再试！");
                    break;
                default:
                    break;
            }
            this.delVisible = false;
        }

    }
}