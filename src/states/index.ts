
import LoginState from './login.state';
import UserState from './user.state';
import BaseState from './base.state';
import CategoryState from './category.state';
import TagState from './tag.state';
import ArticleState from './article.state';


export const stores = {
    base: new BaseState(),
    login: new LoginState(),
    user: new UserState(),
    category: new CategoryState(),
    tag: new TagState(),
    article: new ArticleState()
};

