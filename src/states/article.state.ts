import { action, observable } from 'mobx';
import { message } from 'antd';
import CategoryService from '../services/category.service';
import TagService from '../services/tag.service';
import ArticleService from '../services/article.service';

export default class ArticleState {
    private categoryService: CategoryService;
    private tagService: TagService;
    private articleService: ArticleService;
    constructor() {
        this.categoryService = new CategoryService();
        this.tagService = new TagService();
        this.articleService = new ArticleService();
    }
    //编辑
    @observable form_tags: string = '';
    @observable form_category: number[] = [];
    @observable form_isOriginal: string = '1';
    @observable form_title: string | null = null;
    @observable form_abstract: string | null = null;
    @observable fileList: any[] = [];
    @observable tagInfos: any[] = [];
    @observable categoryInfos: any[] = [];
    @observable status: number = 0;
    //插入标签相关
    @observable pageIndex: number = 1;
    @observable pageSize: number = 10;
    @observable visible: boolean = false;
    @observable loading: boolean = false;
    @observable data: any[] = [];
    @observable pagination: any = {};
    @observable tags: any[] = [];
    //表格相关
    @observable articleData: Array<any> = [];
    @observable articleLoading: boolean = false;
    @observable articlePagination: any = {};
    @observable delVisible: boolean = false;
    @observable id: number = 0;
    //搜索相关
    @observable search_startTime: string | null = null;
    @observable serach_endTime: string | null = null;
    @action.bound
    async getAllCategoryInfos() {
        const res = await this.categoryService.getAllCategoryInfos();
        if (res && res.data) {
            this.categoryInfos = res.data.map(i => {
                return {
                    Id: i.Id,
                    Name: i.CategoryName
                };
            });
        }
    }

    @action.bound
    setStatus(status: number) {
        this.status = status;
    }
    @action.bound
    async addArticle(params, update) {
        params = Object.assign(params, {
            Status: this.status,
        });
        const res = update ? await this.articleService.updateArticle(params) : await this.articleService.addArticle(params);
        if (res && res.data) {
            if (res.data.IsSuccess) {
                window.location.href = "/article/list"
            } else {
                message.warn("失败！请稍后再试！");
            }
        }
    }
    @action.bound
    onShow = () => {
        this.visible = true;
        this.getTagPage();
    }
    @action.bound
    async getTagPage() {
        this.loading = true;
        const res = await this.tagService.getPage(this.pageIndex, this.pageSize);
        this.loading = false;
        if (res && res.data) {
            const pagination = { ...this.pagination };
            pagination.Toal = res.data.TotalRows;
            pagination.showTotal = (total, range) => `${range[0]}-${range[1]} of ${total}`;
            this.data = res.data.Rows;
            this.pagination = pagination;
        }
    }
    @action.bound
    onCancel() {
        this.visible = false;
    }
    @action.bound
    setPageIndex(current: number) {
        this.pageIndex = current;
    }
    @action.bound
    setPageSize(size: number) {
        this.pageSize = size;
    }
    @action.bound
    async  getPage() {
        this.articleLoading = true;
        const res = await this.articleService.getPage(this.pageIndex, this.pageSize, this.search_startTime, this.serach_endTime);
        this.articleLoading = false;
        if (res && res.data) {
            const pagination = { ...this.articlePagination };
            pagination.Toal = res.data.TotalRows;
            pagination.showQuickJumper = true;
            pagination.showSizeChanger = true;
            pagination.showTotal = (total, range) => `${range[0]}-${range[1]} of ${total}`;
            this.articleData = res.data.Rows;
            this.articlePagination = pagination;
        }
    }
    @action.bound
    delonShow(id: number) {
        this.id = id;
        this.delVisible = true;
    }
    @action.bound
    async onOk() {
        const res = await this.articleService.delArticle(this.id);
        if (res && res.data) {
            switch (res.data.Status) {
                case "0":
                    message.warn("删除成功！");
                    await this.getPage();
                    break;
                case "1":
                    message.warn("失败！请稍后再试！");
                    break;
                default:
                    break;
            }
            this.delVisible = false;
        }
    }
    @action.bound
    onHide() {
        this.delVisible = false;
    }
    @action.bound
    async getDetail(id: number) {
        const res = await this.articleService.getArticleDetail(id);
        if (res && res.data) {
            this.form_isOriginal = res.data.ArticleInfo.IsOriginal.toString();
            this.form_title = res.data.ArticleInfo.Title;
            this.form_abstract = res.data.ArticleInfo.Abstract;
            const tempTags = res.data.Tags && res.data.Tags.map(i => {
                return i.Value;
            });
            this.form_tags = tempTags.join(",");
            this.form_category = res.data.Categories && res.data.Categories.map(i => {
                return i.Key;
            });
            this.fileList = res.data.ArticleInfo && res.data.ArticleInfo.ImageUrl && [{
                uid: '-1',
                url: res.data.ArticleInfo.ImageUrl,
                status: 'done'
            }];
            return res.data.ArticleInfo.Content;
        }
        return null;
    }
    @action.bound
    resetFormValue() {
        this.form_abstract = null;
        this.form_category = [];
        this.form_isOriginal = "1";
        this.form_tags = '';
        this.form_title = null;
        this.fileList = [];
    }

}