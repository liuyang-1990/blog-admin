import { observable, action } from 'mobx';
import LoginService from '../services/login.service';
import { message } from 'antd';
export default class LoginState {
    private loginservice: LoginService;
    constructor() {
        this.loginservice = new LoginService();
    }

    @observable loading: boolean = false;


    @action.bound
    handleSubmit(values: any, autoLogin: boolean, redirectUrl: string) {
        this.loginservice.login(values).then(res => {
            this.loading = false;
            if (res && res.data) {
                switch (res.data.Status) {
                    case "0":
                        if (autoLogin) {
                            localStorage.setItem("access_token", res.data.ResultInfo.AccessToken);
                            localStorage.setItem("refresh_token", res.data.ResultInfo.RefreshToken);
                            localStorage.setItem("userInfo",JSON.stringify(res.data.ResultInfo.Profile));
                        } else {
                            sessionStorage.setItem("access_token", res.data.ResultInfo.AccessToken);
                            sessionStorage.setItem("refresh_token", res.data.ResultInfo.RefreshToken);
                            sessionStorage.setItem("userInfo",JSON.stringify(res.data.ResultInfo.Profile));
                        }
                        window.location.href = redirectUrl;
                        break;
                    case "1":
                        message.warn("用户名或者密码错误！");
                        break;
                    case "2":
                        message.warn("用户被禁用！请联系管理员！");
                        break;
                    default:
                        break
                }
            }
        }).catch(err => {
            this.loading = false;
            console.log(err);
            message.warn(err.message);
        });
    }







}