import { action, observable } from 'mobx';
import UserService from '../services/user.service';
import { message } from 'antd';
import { UserAction } from '../model/action.model';

export default class UserState {
    private userService: UserService;
    constructor() {
        this.userService = new UserService();
    }
    //搜索参数
    @observable status: string = "1";
    @observable pageIndex: number = 1;
    @observable pageSize: number = 10;
    @observable userName: string = '';
    //表格
    @observable data: Array<any> = [];
    @observable loading: boolean = false;
    @observable pagination: any = {};
    @observable rowIds: number[] = [];
    @observable visible: boolean = false;
   
    @action.bound
    async getUserPage() {
        this.loading = true;
        const res = await this.userService.getUserPage(this.pageIndex, this.pageSize, this.status, this.userName);
        this.loading = false;
        if (res && res.data) {
            const pagination = { ...this.pagination };
            pagination.Toal = res.data.TotalRows;
            pagination.showQuickJumper = true;
            pagination.showSizeChanger = true;
            pagination.showTotal = (total, range) => `${range[0]}-${range[1]} of ${total}`;
            this.data = res.data.Rows;
            this.pagination = pagination;
            this.visible = false;
        }
        return true;
    }

    setPageIndex(current: number) {
        this.pageIndex = current;
    }

    setPageSize(size: number) {
        this.pageSize = size;
    }

    setUserName(name: string) {
        this.userName = name;
    }

    //改变用户状态（启用，禁用）
    @action.bound
    async  handleChangeStatus(ids) {
        if (!ids || ids.length <= 0) {
            return;
        }
        const res = await this.userService.UpdateStatus({
            ids: ids,
            status: this.status === "1" ? 0 : 1
        });
        if (res && res.data) {
            switch (res.data.Status) {
                case "0":
                    return await this.getUserPage();
                case "1":
                    message.warn("失败！请稍后再试！");
                    break;
                default:
                    break;
            }
        }
    }


    @action.bound
    async addUser(params, userAction: UserAction) {
        let res;
        switch (userAction) {
            case UserAction.Add:
                res = await this.userService.AddUser(params);
                break;
            case UserAction.Update:
                res = await this.userService.UpdateUser(params);
                break;
            default:
                break;
        }
        if (res && res.data) {
            switch (res.data.Status) {
                case "0":
                    await this.getUserPage();
                    break;
                case "1":
                    message.warn("失败！请稍后再试！");
                    break;
                case "2":
                    message.warn("用户已经存在！");
                    break;
                default:
                    break;

            }
        }
    }
    @action.bound
    onToggle() {
        this.visible = !this.visible;
    }
}