import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'mobx-react';
import { ConfigProvider  } from 'antd';
import zh_CN from 'antd/lib/locale-provider/zh_CN';
import { stores } from './states';
import LoadableComponent from './infrastructure/loadableComponent';
import PrivateRoute from './infrastructure/privateRoute';

export default class App extends Component<any, any> {
  render() {
    return (
      <Router>
        <ConfigProvider  locale={zh_CN}>
          <Provider {...stores}>
            <Switch>
             <Route path="/exception/:id" component={LoadableComponent(() => import('./components/Exception/exception'))} />
              <Route key="/login" exact={true} path='/login' component={LoadableComponent(() => import('./components/Login/login'))} />
              <PrivateRoute path="/" component={LoadableComponent(() => import('./layouts/basicLayout'))} />
              <Route component={LoadableComponent(() => import('./components/Exception/exception'))} />
            </Switch>
          </Provider>
        </ConfigProvider>
      </Router>
    )
  }

}