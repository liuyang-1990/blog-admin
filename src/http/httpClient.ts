import axios, { AxiosRequestConfig } from 'axios';
const apis = require('../config/restService.conf.json');

const instance = axios.create({
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
});

if (process.env.NODE_ENV === "development") {
    instance.defaults.baseURL = "http://localhost:49911/api/v1/";
} else if (process.env.NODE_ENV === "production") {
    instance.defaults.baseURL="https://api.nayoung515.top/api/v1/";
}
function getUrl(route: string, ignoreRoute: boolean) {
    return ignoreRoute ? route : apis["resources"][route].url;
}

export function Get(route: string, params?: any, ignoreRoute: boolean = false) {
    const url = getUrl(route, ignoreRoute);
    return instance.get(url, {
        params: params
    })
}

export function Post(route: string, params: any, ignoreRoute: boolean = false, config?: AxiosRequestConfig) {
    const url = getUrl(route, ignoreRoute);
    return instance.post(url, params, config);
}

export function Put(route: string, params: any, ignoreRoute: boolean = false) {
    const url = getUrl(route, ignoreRoute);
    return instance.put(url, params);
}

export function Delete(route: string, params?: any, ignoreRoute: boolean = false) {
    const url = getUrl(route, ignoreRoute);
    return instance.delete(url, {
        params: params
    });
}

// 添加请求拦截器
instance.interceptors.request.use((config) => {
    if (localStorage.getItem("access_token")) {
        config.headers["Authorization"] = localStorage.getItem('access_token');
        config.headers["refresh_token"] = localStorage.getItem('refresh_token');
    }
    return config;
}, err => {
    console.log(err)
    return Promise.reject(err);
});

// 添加响应拦截器
instance.interceptors.response.use(response => {
    if (response.headers["authorization"]) {
        localStorage.setItem('access_token', response.headers["authorization"]);
    }
    return response;
}, err => {
    //未授权
    if (err.response && err.response.status === 401) {
        localStorage.removeItem('access_token');
        localStorage.removeItem('refresh_token');
        window.location.href = "/login";
    } else if (err.response && err.response.status === 500) {
        

    } else if (err.message === "Network Error") {


    }
    return Promise.reject(err);
});

