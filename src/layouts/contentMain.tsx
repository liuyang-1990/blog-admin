import React, { Component } from 'react';
import { withRouter, Switch, Route } from 'react-router-dom';
import { routes } from '../config/route.conf';
import LoadableComponent from '../infrastructure/loadableComponent';


class ContentMain extends Component<any, any>{
    render() {
        return (
            <Switch>
                {
                    routes && routes.map(item => {
                        return <Route
                            key={item.path}
                            path={item.path}
                            exact={true}
                            component={LoadableComponent(item.component)}
                        />
                    })
                }
                <Route component={LoadableComponent(() => import('../components/Exception/exception'))} />
            </Switch>
        );
    }
}

export default withRouter(ContentMain);