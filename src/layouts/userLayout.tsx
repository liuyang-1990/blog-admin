import React, { Component, Fragment } from 'react';
import { Icon } from 'antd';
import './userLayout.less';
import logo from '../assests/images/logo.svg';
import Footer from './footer';

const copyright = (
    <Fragment>
        Copyright <Icon type="copyright" /> 2019 created by liuyang | <a href="http://www.beian.miit.gov.cn">豫ICP备18041733号-1</a>
    </Fragment>
);
class UserLayout extends Component<any, any> {
    render() {
        return (
            <div className="login_container">
                <div className="content">
                    <div className="top">
                        <div className="login_header">
                            <img alt="logo" className="login_logo" src={logo} />
                            <span className="title">Blog Admin</span>
                        </div>
                        <div className="desc">博客后台管理系统</div>
                    </div>
                    {this.props.children}
                </div>
                <Footer copyright={copyright} />
            </div>


        )
    }
}

export default UserLayout;