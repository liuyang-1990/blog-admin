import React, { Component, Fragment } from 'react';
import { BackTop, Icon } from 'antd';
import ProLayout, { MenuDataItem } from '@ant-design/pro-layout';
import { Link } from 'react-router-dom';
import logo from '../assests/images/logo.svg';
import Footer from './footer';
import ContentMain from './contentMain';
import { menus } from '../config/menus.conf';
import RightContent from './rightContent';


export default class BasicLayout extends Component<any, any> {
    render() {

        const menuDataRender = (menuList: MenuDataItem[]): MenuDataItem[] => {
            return menuList.map(item => {
                const localItem = { ...item, children: item.children ? menuDataRender(item.children) : [] };
                return localItem as MenuDataItem;
            });
        };

        const links = [{
            key: 'Ant Design Pro',
            title: 'Ant Design Pro',
            href: 'https://pro.ant.design',
            blankTarget: true,
        },
        {
            key: 'github',
            title: <Icon type="github" />,
            href: 'https://gitlab.com/liuyang-1990/blog-admin',
            blankTarget: true,
        },
        {
            key: '豫ICP备18041733号-1',
            title: 'Ant Design',
            href: 'https://www.beian.miit.gov.cn',
            blankTarget: true,
        }];

        const copyright = (
            <Fragment>
                Copyright <Icon type="copyright" /> 2019 created by liuyang
            </Fragment>
        );
        return (
            <ProLayout
                logo={logo}
                title="Blog Admin"
                fixSiderbar={true}
                fixedHeader={true}
                route={menus}
                menuItemRender={(menuItemProps, defaultDom) => {
                    return <Link key={menuItemProps.path} to={menuItemProps.path}>{defaultDom}</Link>;
                }}
                rightContentRender={rightProps => <RightContent {...rightProps} />}
                footerRender={footerProps => <Footer links={links} copyright={copyright} {...footerProps} />}
                menuDataRender={menuDataRender}
                {...this.props}
            >
                <BackTop visibilityHeight={100} />
                <ContentMain />
            </ProLayout>
        )
    }
}
