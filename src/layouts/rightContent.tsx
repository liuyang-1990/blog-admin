import React, { Component } from 'react';
import { Avatar, Menu, Icon, Dropdown } from 'antd';
import { withRouter } from 'react-router-dom';
import "./rightContent.less";
import storageUtils from '../infrastructure/utils/storageUtils';

class RightContent extends Component<any, any>{

    logout = () => {
        localStorage.clear();
        sessionStorage.clear();
        this.props.history.push("/login");
    }

    render() {
        const currentUserStr = storageUtils.getItem("userInfo") || "";
        const currentUser = JSON.parse(currentUserStr);

        const menu = (
            <Menu className={"menu"} selectedKeys={[]}>
                <Menu.Item key="userCenter">
                    <Icon type="user" />
                    个人中心
                </Menu.Item>
                <Menu.Item key="userinfo">
                    <Icon type="setting" />
                    个人设置
                </Menu.Item>
                <Menu.Divider />
                <Menu.Item key="logout" onClick={this.logout}>
                    <Icon type="logout" />
                    退出登录
                </Menu.Item>
            </Menu>
        );

        return (
            <div className="right">
                <Dropdown overlay={menu} overlayClassName="container">
                    <span className="action account">
                        <Avatar
                            size="small"
                            className="avatar"
                            src={currentUser.Avatar}
                            alt="avatar" />
                        <span className="name">{currentUser.UserName}</span>
                    </span>
                </Dropdown>
            </div>
        )
    }
}

export default withRouter(RightContent)