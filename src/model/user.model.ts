export interface IUserInfo {
    Id?: number
    UserName?: string,
    Role: number,
    Status: number,
    CreateTime?: string
}
