import { Post } from '../http/httpClient';

export default class LoginService {
   
    public login(params) {
        return Post("loginApi",params);
    }

}