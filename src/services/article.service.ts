import { Post, Get, Delete, Put } from "../http/httpClient";

export default class ArticleService {

    public addArticle(params: any) {
        return Post("addArticleApi", params)
    }

    public updateArticle(params: any) {
        return Put("addArticleApi", params)
    }

    public getPage(pageIndex: number, pageSize: number, start?: string | null, end?: string | null) {
        return Get("articlePageApi", {
            PageNum: pageIndex,
            PageSize: pageSize,
            StartTime: start,
            EndTime: end
        })
    }

    public delArticle(id: number) {
        return Delete("article/" + id, null, true);
    }

    public getArticleDetail(id: number) {
        return Get("article/" + id, null, true);
    }
}