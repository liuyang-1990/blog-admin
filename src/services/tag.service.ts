import { Get, Post, Put, Delete } from '../http/httpClient';

export default class TagService {

    public getPage(pageIndex: number, pageSize: number, tagName?: string) {
        return Get("tagPageApi", {
            PageNum: pageIndex,
            PageSize: pageSize,
            tagName: tagName
        });
    }

    public addTag(params: any) {
        return Post("addtagApi", params);
    }

    public UpdateTag(params) {
        return Put("addtagApi", params);
    }

    public deleteTag(id: number) {
        return Delete("tag/" + id, null, true);
    }

    public getAllTagInfos() {
        return Get("allTagApi");
    }

}