import { Get, Post, Put, Delete } from '../http/httpClient';

export default class CategoryService {

    public getPage(pageIndex: number, pageSize: number, categoryName: string) {
        return Get("categoryPageApi", {
            PageNum: pageIndex,
            PageSize: pageSize,
            categoryName: categoryName
        });
    }

    public addCategory(params: any) {
        return Post("addCategoryApi", params);
    }

    public UpdateCategory(params) {
        return Put("addCategoryApi", params);
    }

    public deleteCategory(id: number) {
        return Delete("category/" + id, null, true);
    }

    public getAllCategoryInfos(){
        return Get("allCategoryApi");
    }

}