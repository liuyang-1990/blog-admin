import { Get, Post, Put } from '../http/httpClient';

export default class UserService {

    public getUserPage(pageIndex: number, pageSize: number, status: string, userName: string) {
        return Get("userPageApi", {
            UserName: userName,
            Status: status,
            PageNum: pageIndex,
            PageSize: pageSize
        });
    }

    public AddUser(params) {
        return Post("addUserApi", params);
    }


    public UpdateUser(params) {
        return Put("addUserApi", params);
    }

    public ChangePassword(params) {
        return Post("chgPasswordApi", params);
    }

    public UpdateStatus(params){
        return Post("updateStatusApi", params);
    }

}