const menus: any =
{
    routes: [
        {
            name: '首页',
            icon: 'home',
            path: '/'
        },
        {
            name: '文章',
            icon: 'book',
            path: '/article',
            routes: [
                { path: '/article/addArticle', name: '发文', icon: 'highlight', },
                { path: '/article/list', name: '列表', icon: 'form', },
            ]
        },
        {
            name: '标签',
            icon: 'tag',
            path: '/tag/list',
        },
        {
            name: '分类',
            icon: 'bars',
            path: '/category/list',
        },
        {
            name: '用户',
            icon: 'user',
            path: '/user/list',
        }
    ]
}


export { menus };