
const routes: any[] = [
    {
        component: () => import('../components/Home/home'),
        path: '/'
    },
    {
        component: () => import('../components/Article/addArticle'),
        path: '/article/addArticle'
    },
    {
        component: () => import('../components/Article/articleList'),
        path: '/article/list'
    },
    {
        component: () => import('../components/Tag/tagInfo'),
        path: '/tag/list'
    },
    {
        component: () => import('../components/Category/categoryInfo'),
        path: '/category/list'
    },
    {
        component: () => import('../components/User/userInfo'),
        path: '/user/list'
    }

];
export { routes };
